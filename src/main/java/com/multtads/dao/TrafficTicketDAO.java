package com.multtads.dao;

import java.util.List;

import com.multtads.model.TrafficTicket;
import com.multtads.util.HibernateUtil;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import org.hibernate.criterion.Restrictions;

public class TrafficTicketDAO {

    // post
    public boolean insert(TrafficTicket trafficTicket) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(trafficTicket);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // get
    public TrafficTicket get(int id) {
        TrafficTicket trafficTicket = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(TrafficTicket.class);
            criteria.add(Restrictions.eq("id", id));

            trafficTicket = (TrafficTicket) criteria.uniqueResult();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return trafficTicket;
    }

    public List<Object[]> allOrderByDate() {
        List<Object[]> results = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(TrafficTicket.class)
                    .setProjection(Projections.projectionList().add(Projections.groupProperty("ticket_date")))
                    .addOrder(Order.desc("ticket_date"));
            results = criteria.list();
            displayObjectsList(results);
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return results;
    }

    public void displayObjectsList(List list) {
        Iterator iter = list.iterator();
        if (!iter.hasNext()) {
            System.out.println("No objects to display.");
            return;
        }
        while (iter.hasNext()) {
            System.out.println("New object");
            Object[] obj = (Object[]) iter.next();
            for (int i = 0; i < obj.length; i++) {
                System.out.println(obj[i]);
            }

        }
    }

    // all
    public List<TrafficTicket> all() {
        List<TrafficTicket> trafficTickets = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(TrafficTicket.class);
            trafficTickets = criteria.list();

            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return trafficTickets;
    }
}
