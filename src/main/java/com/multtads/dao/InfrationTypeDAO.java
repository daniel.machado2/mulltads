package com.multtads.dao;

import java.util.List;

import com.multtads.model.InfractionType;
import com.multtads.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.criterion.Restrictions;

public class InfrationTypeDAO {

    // post
    public boolean insert(InfractionType infration) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(infration);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // get
    public InfractionType get(int id) {
        InfractionType infration = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(InfractionType.class);
            criteria.add(Restrictions.eq("id", id));

            infration = (InfractionType) criteria.uniqueResult();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return infration;
    }

    // all
    public List<InfractionType> all() {
        List<InfractionType> infrations = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(InfractionType.class);
            infrations = criteria.list();

            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return infrations;
    }

    // update
    public boolean update(InfractionType infration) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(infration);
            session.getTransaction().commit();
            session.clear();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean delete(InfractionType infration) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.delete(infration);
            tx.commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
