/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.multtads.dao;

import java.util.List;

import com.multtads.model.Payment;
import com.multtads.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Danie
 */
public class PaymentDAO {
    
    // post
    public boolean insert(Payment payment) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(payment);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // get
    public Payment get(int id) {
        Payment payment = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Payment.class);
            criteria.add(Restrictions.eq("id", id));

            payment = (Payment) criteria.uniqueResult();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return payment;
    }

    // all
    public List<Payment> all() {
        List<Payment> payments = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria criteria = session.createCriteria(Payment.class);
            payments = criteria.list();

            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return payments;
    }

    // update
    public boolean update(Payment payment) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(payment);
            session.getTransaction().commit();
            session.clear();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean delete(Payment payment) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            session.delete(payment);
            tx.commit();
            session.close();
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
