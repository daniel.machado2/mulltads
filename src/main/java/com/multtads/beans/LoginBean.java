/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.multtads.beans;

import com.multtads.dao.TrafficTicketDAO;
import com.multtads.model.User;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author daniel
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String email;
    private String password;
    private User user;

    /**
     * Creates a new instance of LoginManbe
     */
    public LoginBean() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isLoged() {
        return this.getEmail() != null;
    }

    public String verifyLogin() {
//        String pass = MDFive.encripta(this.getPassword());
//        System.out.println(MDFive.encripta(this.getPassword()));
//        usuario = LoginFacade.FazerLogin(this.getLogin(), pass);
        User usuario = new User("daniel@gmail.com", "088", "Daniel", 'p');
        TrafficTicketDAO trafficTicketDAO = new TrafficTicketDAO();
        List lista = trafficTicketDAO.allOrderByDate();
        System.out.println("AAAAAA");
        if (usuario != null) {
            switch (usuario.getType()) {
                case 'p':
                    return "admin-dashboard";
                default:
                    return "login";
            }
        }
        return "login";
    }

    @PostConstruct
    public void init() {
        user = new User();
    }

    public String home() {
        if (user.getType() == 'e') {
            return "entregador";
        } else if (user.getType() == 'g') {
            return "gerente";
        }
        return null;
    }
}
