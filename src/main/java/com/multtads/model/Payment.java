/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.multtads.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author daniel
 */
@Entity
public class Payment implements Serializable {

    private Long id;
    private Date date;
    private Integer paymentDelaysDay;
    private Float price;
    private Infration infration;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "payment_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "payment_date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column(name = "payment_days_delay")
    public Integer getPaymentDelaysDay() {
        return paymentDelaysDay;
    }

    public void setPaymentDelaysDay(Integer paymentDelaysDay) {
        this.paymentDelaysDay = paymentDelaysDay;
    }

    @Column(name = "price")
    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "infraction_id")
    public Infration getInfration() {
        return infration;
    }

    public void setInfration(Infration infration) {
        this.infration = infration;
    }

}
