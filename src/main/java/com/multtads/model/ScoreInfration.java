package com.multtads.model;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Danie
 */
public enum ScoreInfration {
    SEVEN(7), FIVE(5), THREE(3), ONE(1);

    private int numVal;

    ScoreInfration(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }
}
