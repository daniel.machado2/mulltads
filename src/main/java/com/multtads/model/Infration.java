/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.multtads.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Danie
 */
@Entity
@Table(name = "infraction")
public class Infration implements Serializable {

    private Long id;
    private String descriptionInfrigment;
    private InfractionType infractionType;

    private TrafficTicket trafficTicket;
    private Payment payment;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "infraction_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "decription_infringement")
    public String getDescriptionInfrigment() {
        return descriptionInfrigment;
    }

    public void setDescriptionInfrigment(String descriptionInfrigment) {
        this.descriptionInfrigment = descriptionInfrigment;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "infraction_type_id")
    public InfractionType getInfractionType() {
        return infractionType;
    }

    public void setInfractionType(InfractionType infractionType) {
        this.infractionType = infractionType;
    }

    @OneToOne(mappedBy = "infration")
    public TrafficTicket getTrafficTicket() {
        return trafficTicket;
    }

    public void setTrafficTicket(TrafficTicket trafficTicket) {
        this.trafficTicket = trafficTicket;
    }

    @OneToOne(mappedBy = "infration")
    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

}
