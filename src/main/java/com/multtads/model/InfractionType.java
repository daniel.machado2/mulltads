/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.multtads.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Danie
 */
@Entity
@Table(name = "infraction_type")
public class InfractionType implements Serializable {

    private Long id;
    private Float price;
    private DescriptionType descriptionType;
    private ScoreInfration scoreInfration;

    private Infration infration;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "infraction_type_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "price", nullable = false)
    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Column(name = "description_type")
    public DescriptionType getDescriptionType() {
        return descriptionType;
    }

    public void setDescriptionType(DescriptionType descriptionType) {
        this.descriptionType = descriptionType;
    }

    @Column(name = "score")
    public ScoreInfration getScoreInfration() {
        return scoreInfration;
    }

    public void setScoreInfration(ScoreInfration scoreInfration) {
        this.scoreInfration = scoreInfration;
    }

    @OneToOne(mappedBy = "infractionType")
    public Infration getInfration() {
        return infration;
    }

    public void setInfration(Infration infration) {
        this.infration = infration;
    }

}
