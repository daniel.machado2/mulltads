 drop database mulltads ;
create database if not exists mulltads;
use mulltads;

create table if not exists addresses(
	address_id int auto_increment,
	street varchar(50) not null, 
	number int not null,
	complement varchar(50), 
	neighborhood varchar(50), 
	zip_code varchar(11),
	city varchar(30), 
	state varchar(30),
    CONSTRAINT pk_address PRIMARY KEY (address_id)
);

create table if not exists infraction_type(
	infraction_type_id int auto_increment,
	description_type enum('Gravissima', 'Grave', 'Media', 'Leve'),
    score enum('7', '5', '3', '1'),
    price float,
    CONSTRAINT pk_infraction_type PRIMARY KEY (infraction_type_id)
);

create table if not exists infraction(
	infraction_id int auto_increment,
	decription_infringement text,
    infraction_type_id int not null,
    foreign key (infraction_type_id) references infraction_type(infraction_type_id),
	CONSTRAINT pk_infraction PRIMARY KEY (infraction_id)
);

create table if not exists traffic_ticket(
	traffic_ticket_id int auto_increment,
	renavam varchar(50) not null, 
	cpf varchar(14) unique,
	description_ticket text,
	ticket_date date,
	address_id int,
    infraction_id int,
    foreign key (address_id) references addresses(address_id),
    foreign key (infraction_id) references infraction(infraction_id),
    CONSTRAINT pk_traffic_ticket PRIMARY KEY (traffic_ticket_id)
);

create table if not exists users(
	user_id integer auto_increment not null,
	email VARCHAR(30),
	senha varchar(100),
	cpf varchar(14),
	rg varchar(12),
	nome varchar(50),
	tipo enum('f','p', 'a'),
	CONSTRAINT pk_user PRIMARY KEY (user_id)
);

create table if not exists payment(
 payment_id integer auto_increment not null,
 payment_date date,
 payment_days_delay int,
 price float,
 infraction_id int,
 constraint pk_payment primary key (payment_id),
 foreign key (infraction_id) references infraction(infraction_id)
);

alter table users add address_id integer references addresses(address_id);

insert into addresses(street, number, complement,neighborhood,zip_code,city,state) values ("Avenida Principal", 3, "", "Centro", "80.010-130", "Mundial", "MD");
insert into users(cpf, nome, rg, email, address_id, tipo) values ('088.824.129-11', 'Rafael', '32.639.581-7', 'teste@gmail.com', 1, 'p');
insert into infraction_type(description_type, score, price) values 
('Gravissima', '7', 512), ('Grave', '5', 132), ('Media', '3', 100);

insert into infraction(decription_infringement, infraction_type_id) values 
('condutor alterado', 1),('batida de carro', 2), ('parou no lugar errado', 3);

insert into infraction(decription_infringement, infraction_type_id) values ('texto muito massa', 1);
insert into payment(payment_date, payment_days_delay, price, infraction_id) values (current_date, 0, 400, 1);

insert into traffic_ticket(renavam, cpf, description_ticket, ticket_date, address_id, infraction_id) values 
('123', '088.824.129-11', 'descrição de multa', current_date, 1, 1),
('345', '088.823.129-11', 'descrição de multa', current_date, 1, 2),
('234', '088.822.129-11', 'descrição de multa', '2020-06-26', 1, 3)
;

select * from traffic_ticket group by month(ticket_date) order by ticket_date desc;